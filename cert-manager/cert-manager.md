## Let's encrypt cert-manager quick reference

#### Haproxy configuration
```sh
frontend http_front
  bind *:443
  mode tcp
  option tcplog
  default_backend http_back

backend http_back
  mode tcp
  balance roundrobin
  server kworker1 <kworker1-ip>:443
  server kworker2 <kworker2-ip>:443
```

#### Install helm
```sh
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
```

*Wait for tiller component to be active*

#### cert-manager setup
```sh
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.4.0/cert-manager.crds.yaml
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --create-namespace \
  --version v1.4.0 \
  # --set installCRDs=true
kubectl get pods --namespace cert-manager
```

*Wait for cert-manager pods to be active*

#### create secret
```sh
echo -n "${AWS_SECRET_ACCESS_KEY}" | base64 
# grab your output 'OUTPUT_AWS_SECRET_ACCESS_KEY_BASE64'
sed -i 's/${AWS_SECRET_ACCESS_KEY_BASE64}/"OUTPUT_AWS_SECRET_ACCESS_KEY_BASE64"/' kubernetes-demo-23062021/cert-manager/cert-manager-secret.yaml
kubectl apply -f kubernetes-demo-23062021/cert-manager/cert-manager-secret.yaml
```

#### Apply ClusterIssuer on Kubernetes cluster
```sh
git clone https://gitlab.com/estabilis/devops-bootcamp/kubernetes-demo-23062021.git
sed -i "s/${BASE_DOMAIN}/${YOUR_BASE_DOMAIN}/" kubernetes-demo-23062021/cert-manager/cluster-issuer.yaml
sed -i "s/${AWS_CERTIFICATE_ZONE}/${YOUR_CERTIFICATE_REGION}/" kubernetes-demo-23062021/cert-manager/cluster-issuer.yaml
sed -i "s/${HOSTED_ZONE_ID}/${YOUR_HOSTED_ZONE_ID}/" kubernetes-demo-23062021/cert-manager/cluster-issuer.yaml
sed -i "s/${AWS_ACCSESS_KEY_ID}/${YOUS_AWS_ACCESS_KEY_ID}/" kubernetes-demo-23062021/cert-manager/cluster-issuer.yaml
kubectl apply -f kubernetes-demo-23062021/cert-manager/cluster-issuer.yaml
kubectl apply -f kubernetes-demo-23062021/cert-manager/certificate.yaml
```


