# Summary DevOps Bootcamp by Estabilis

## how to install kubernetes on-premises
- [kubernetes](https://gitlab.com/estabilis/devops-bootcamp/kubernetes-demo-23062021/-/blob/main/kubernetes/kubernetes-setup.md)

## how to install helm on kubernetes
- [helm](https://gitlab.com/estabilis/devops-bootcamp/kubernetes-demo-23062021/-/blob/main/helm/setup-helm.md)

## how to install cert-manager on kubernetes and create ClusterIssuer
- [certmanager](https://gitlab.com/estabilis/devops-bootcamp/kubernetes-demo-23062021/-/blob/main/cert-manager/cert-manager.md)

## how to install Nginx Ingress-controller
- [ingress-controller](https://gitlab.com/estabilis/devops-bootcamp/kubernetes-demo-23062021/-/blob/main/ingress-controller/ingress-controller-setup.md)

## deploy a simple POD aplication and Ingress resource
- [resources](https://gitlab.com/estabilis/devops-bootcamp/kubernetes-demo-23062021/-/blob/main/resources/resources.md)
