## Apply pod application nginx-estabilis and service

```sh
kubectl apply -f kubernetes-demo-23062021/resources/nginx-deployment.yaml
```

## Apply ingress resource application nginx-estabilis

```sh
kubectl apply -f kubernetes-demo-23062021/resources/ingress-nginx-resource.yaml
```

## check pod running

```sh
kubectl get po
```

# check ingress 

```sh
kubectl get ingress
```
