# Nginx Ingress-controller Bare-metal

## Using NodePort:

```sh
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx
```

# Verify installation

## To check if the ingress controller pods have started, run the following command:

```sh
kubectl get pods -n ingress-nginx \
  -l app.kubernetes.io/name=ingress-nginx --watch
```

## To check NodePort Service

```sh
kubectl get svc -n ingress-nginx
```
